package com.twasyl.envviewer.controllers;

import com.twasyl.envviewer.beans.Property;
import com.twasyl.envviewer.beans.Property.Type;
import com.twasyl.envviewer.controls.PTableColumn;
import java.net.URL;
import java.util.Map;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableView;
import javafx.scene.layout.Pane;

/**
 *
 * @author thierry
 */
public class EnvViewerController implements Initializable {
    
    @FXML
    private Pane rootPane;
    @FXML
    private TableView<Property> propertiesTable;
    @FXML
    private PTableColumn<Property, String> nameColumn;
    @FXML
    private PTableColumn<Property, String> valueColumn;
    @FXML
    private PTableColumn<Property, Type> typeColumn;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        for(Map.Entry entry : System.getProperties().entrySet()) {
            propertiesTable.getItems().add(new Property(entry.getKey().toString(), entry.getValue().toString(), Type.SYSTEM));
        }
        
        for(Map.Entry entry : System.getenv().entrySet()) {
            propertiesTable.getItems().add(new Property(entry.getKey().toString(), entry.getValue().toString(), Type.ENVIRONMENT));
        }
    }    
}
