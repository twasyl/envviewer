package com.twasyl.envviewer.app;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author thierry
 */
public class EnvViewer extends Application {
    
    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("/com/twasyl/envviewer/resources/fxml/EnvViewer.fxml"));
        
        Scene scene = new Scene(root);
        
        stage.setScene(scene);
        stage.setTitle("EnvViewer");
        stage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
