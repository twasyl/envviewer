package com.twasyl.envviewer.beans;

import java.io.Serializable;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * Represents a system or environment property.
 * @author twasyl
 */
public class Property implements Serializable {
    
    public enum Type {
        SYSTEM, ENVIRONMENT
    }
    
    private final StringProperty name = new SimpleStringProperty();
    private final StringProperty value = new SimpleStringProperty();
    private final ObjectProperty<Type> type = new SimpleObjectProperty<>();
    
    public Property() {
    }
    
    public Property(String name, String value, Type type) {
        this.name.set(name);
        this.value.set(value);
        this.type.set(type);
    }
    
    public final StringProperty nameProperty() {
        return this.name;
    }
    
    public final String getName() {
        return this.nameProperty().get();
    }
    
    public final void setName(String name) {
        this.nameProperty().set(name);
    }
    
    public final StringProperty valueProperty() {
        return this.value;
    }
    
    public final String getValue() {
        return this.valueProperty().get();
    }
    
    public final void setValue(String value) {
        this.valueProperty().set(value);
    }
    
    public final ObjectProperty<Type> typeProperty() {
        return this.type;
    }
    
    public final Type getType() {
        return this.typeProperty().get();
    }
    
    public final void setType(Type type) {
        this.typeProperty().set(type);
    }
}
